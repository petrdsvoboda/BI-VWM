import * as React from 'react'
import axios from 'axios'

import { withStyles, WithStyles } from 'material-ui/styles'
import CssBaseline from 'material-ui/CssBaseline'
import IconButton from 'material-ui/IconButton'
import Snackbar from 'material-ui/Snackbar'
import CloseIcon from '@material-ui/icons/Close'

import Movie from '../models/Movie'

import Search from './Search'
import Results from './Results'

export interface RequestData {
	movies: Movie[]
	time: number,
	count: number
}

interface State {
	[key: string]: RequestData | boolean | string
	threshold: RequestData
	sequential: RequestData
	newSearch: boolean
	error: string
}

type ClassKeys = 'root' | 'close'

const decorate = withStyles<ClassKeys>((theme) => ({
	root: {
		display: 'flex',
		alignItems: 'flex-start',
		justifyContent: 'center',
		height: '100%',
		overflow: 'hidden',
		paddingBottom: 32,
		paddingTop: 32
	},
	close: {
		width: theme.spacing.unit * 4,
		height: theme.spacing.unit * 4,
	}
}))

export interface Request {
	aggregate: string
	count: number
	attributes: string[]
	order: 'desc' | 'asc'
}

/**
 * Root component of aplpication, manages all requests, displays search and result movie list
 * 
 * @class App
 * @extends {React.Component<WithStyles<ClassKeys>, State>}
 */
class App extends React.Component<WithStyles<ClassKeys>, State> {
	state: State = {
		threshold: {
			movies: [],
			time: 0,
			count: 0
		},
		sequential: {
			movies: [],
			time: 0,
			count: 0
		},
		newSearch: false,
		error: ''
	}

	/**
	 * Sends request to server and sets state to response data
	 * 
	 * @memberof App
	 * @param {'threshold' | 'sequential'} type Type of request
	 * @param {Request} requestData Request params
	 */
	requestData = async (type: 'threshold' | 'sequential', requestData: Request) => {
		try {
			const response = await axios.post<RequestData>(
				'http://localhost:3001/movies/' + type,
				requestData,
				{ timeout: 2000 }
			)
		
			this.setState({
				[type]: response.data,
				newSearch: true
			})
		} catch (err) {
			this.setState({
				error: 'Error while searching for movies.'
			})
		}
	}

	/**
	 * Sends sequential and threshold request
	 * 
	 * @memberof App
	 * @param {Request} requestData Request params
	 */
	getData = async (requestData: Request) => {
		this.requestData('threshold', requestData)
		this.requestData('sequential', requestData)
	}

	unsetNewSearch = () => { this.setState({ newSearch: false }) }
	closeErrors = () => { this.setState({ error: '' }) }

	render() {
		return(
			<div className={this.props.classes.root}>
				<CssBaseline />
				<Search getData={this.getData} />
				<Results
					newSearch={this.state.newSearch}
					unsetNewSearch={this.unsetNewSearch}
					thresholdData={this.state.threshold}
					sequentialData={this.state.sequential}
				/>
				<Snackbar
					anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
					open={this.state.error.length > 0}
					autoHideDuration={3000}
					onClose={this.closeErrors}
					message={<span>{this.state.error}</span>}
					action={
						<IconButton
							color="inherit"
							className={this.props.classes.close}
							onClick={this.closeErrors}
						>
							<CloseIcon />
						</IconButton>
					}
				/>
			</div>
		)
	}
}

export default decorate<{}>(App)
